package byteis.com.trainingandroid.models;

/**
 * Created by Dina on 12/10/2016.
 */

public class Product {

    public Product(String title, String description, String company, String numInStore, String availableCountry) {
        this.title = title;
        this.description = description;
        this.company = company;
        this.numInStore = numInStore;
        this.availableCountry = availableCountry;
    }

    private String title;

    private String description;

    private String company;

    private String numInStore;

    private String availableCountry;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAvailableCountry() {
        return availableCountry;
    }

    public void setAvailableCountry(String availableCountry) {
        this.availableCountry = availableCountry;
    }

    public String getNumInStore() {
        return numInStore;
    }

    public void setNumInStore(String numInStore) {
        this.numInStore = numInStore;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
