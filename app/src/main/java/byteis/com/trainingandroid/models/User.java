package byteis.com.trainingandroid.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

import byteis.com.trainingandroid.util.UtilityMethods;

/**
 * Created by Dina on 09/10/2016.
 */

@Table(name = "User")
public class User extends Model {

    @Column(name = "email")
    @SerializedName("email")
    private String email;

    @Column(name = "password")
    @SerializedName("password")
    private String password;

    @SerializedName("access_token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUserData(String email, String password) {
        this.email = email;
        if (password != null)
            this.password = password;
    }

    /*Save the model to database*/
    public Long saveUser() {
        return save();
    }

    /* User Information Validations*/
    public boolean isValidEmail() {
        if (email == null || "".equals(email.trim()) || !UtilityMethods.isEmailValid(email))
            return false;
        return true;
    }

    public boolean isValidPassword() {
        if (password == null || "".equals(password.trim()))
            return false;
        return true;
    }
}
