package byteis.com.trainingandroid.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.transition.Transition;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import byteis.com.odooapilibrary.api.ApiClient;
import byteis.com.odooapilibrary.api.ApiClientImp;
import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.data.SharedPref;
import byteis.com.trainingandroid.databinding.ActivityBaseBinding;
import byteis.com.trainingandroid.managers.UsersManager;
import byteis.com.trainingandroid.models.ServerResponse;
import byteis.com.trainingandroid.util.UtilityMethods;

/**
 * Created by Dina on 09/10/2016.
 */

public class BaseActivity extends AppCompatActivity {

    protected ActivityBaseBinding baseBinding;
    protected ApiClient apiClient;
    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_base, null, false);
        setContentView(baseBinding.getRoot());
        apiClient = ApiClientImp.getInstance(this, UtilityMethods.ODOO_DATABASE_NAME);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_color_dark));
        }

        applyTransition(this);
        initToolbar();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected Activity applyTransition(Activity activity) {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            Transition transition = new Slide();
            ((Slide) transition).setSlideEdge(Gravity.RIGHT);
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            transition.excludeTarget(android.R.id.navigationBarBackground, true);
            activity.getWindow().setEnterTransition(transition);
//            activity.getWindow().setExitTransition(transition);
        }
        return activity;
    }

    private void initToolbar() {
        if (baseBinding.toolbar != null)
            setSupportActionBar(baseBinding.toolbar.toolbar);
    }

    protected void inflateContent(View view) {
        RelativeLayout contentContainerLayout = baseBinding.baseCustomView;
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        contentContainerLayout.removeAllViews();
        contentContainerLayout.addView(view);
    }

    protected void setError(TextInputLayout textInputLayout, String error) {
        textInputLayout.setError(error);
        textInputLayout.getEditText().setError(error);
    }

    protected void showProgressDialog(String dialogMessage, boolean isCancelable, boolean isIndeteminate) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(this);

        if (!progressDialog.isShowing()) {
            progressDialog.setMessage(dialogMessage);
            progressDialog.setCancelable(isCancelable);
            progressDialog.setIndeterminate(isIndeteminate);
            progressDialog.show();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startActivityWithTransition(Activity activity, Intent intent) {
        ActivityOptionsCompat options = null;
        try {
            options = getOptionsCompat(activity);
            ActivityCompat.startActivity(activity, intent, options.toBundle());
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    private ActivityOptionsCompat getOptionsCompat(Activity activity) {
        ActivityOptionsCompat options = null;

        try {
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, null);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        applyTransition(activity);
        return options;
    }

    protected void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    protected void onServerFailure(ServerResponse serverResponse, int statusCode) {
        String errorMessage = getString(R.string.something_wrong);
        if (serverResponse != null && serverResponse.getErrorMessageStr() != null && !"".equals((serverResponse.getErrorMessageStr()).trim()))
            errorMessage = serverResponse.getErrorMessageStr();

        if (!isFinishing()) {
            UtilityMethods.showSnackbar(errorMessage, getWindow().getDecorView(), this);
            dismissProgressDialog();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void finishActivity(Activity activity) {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            activity.finishAfterTransition();
        } else {
            activity.finish();
        }
    }

    protected void setToolBarContent(String title, boolean showTitle, boolean showHome) {
        if (!"".equals(title)) {
            getSupportActionBar().setTitle(title);
        }
        getSupportActionBar().setDisplayShowTitleEnabled(showTitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(showHome);
    }

    protected void signout(){
        SharedPref.deleteShared(BaseActivity.this);
        UsersManager.getInstance().removeLoginUser();
        Intent i = new Intent(BaseActivity.this, LoginActivity.class);
        startActivityWithTransition(BaseActivity.this, i);
        finishActivity(BaseActivity.this);
    }
}
