package byteis.com.trainingandroid.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;
import byteis.com.odooapilibrary.api.ClientCallback;
import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.data.SharedPref;
import byteis.com.trainingandroid.databinding.ActivityLoginBinding;
import byteis.com.trainingandroid.managers.EncryptionManager;
import byteis.com.trainingandroid.util.UtilityMethods;

public class LoginActivity extends BaseActivity implements ClientCallback {

    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_login, null, false);
        inflateContent(binding.getRoot());
    }

    public void onLoginClicked(View view) {
        resetValidatinErrors();
        if (isValidData()) {
            showProgressDialog(getString(R.string.please_wait), false, true);
            apiClient.odooLogin(binding.usernameEdittext.getText().toString(), binding.password.getText().toString(), this);
        }
    }

    private void resetValidatinErrors() {
        setError(binding.usernameTextInput, null);
        setError(binding.passwordTextInput, null);
    }

    private boolean isValidData() {
        boolean isValid = true;
        if (binding.usernameEdittext.getText().toString().trim().isEmpty()) {
            setError(binding.usernameTextInput, getString(R.string.invalid_username));
            isValid = false;
        }

        if (binding.password.getText().toString().trim().isEmpty()) {
            setError(binding.passwordTextInput, getString(R.string.invalid_password));
            isValid = false;
        }

        return isValid;
    }

    @Override
    public void onServerResultSuccess(Object object) {
        if (object != null) {
            JSONObject jsonObject = (JSONObject) object;
            Object uid = jsonObject.opt("uid");
            if (uid != null && uid instanceof Integer) {
                try {
                    SharedPref.SaveString(this, SharedPref.SESSION_ID, jsonObject.getString("session_id"));
                    SharedPref.SaveInt(this, SharedPref.USER_ID, jsonObject.optInt("uid"));

                    /*Saving encrypted user password -- todo try to make this with ndk*/
                    if (binding.password.getText() != null) {
                        String passwordText = binding.password.getText().toString();
                        EncryptionManager.savePassword(passwordText, LoginActivity.this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (!isFinishing()) {
                    ActivityCompat.finishAffinity(this);
                    startActivityWithTransition(this, new Intent(this, MainActivity.class));
                }
            } else
                serverFailure(getString(R.string.something_wrong));
        }
    }

    private void serverFailure(String failureMessage) {
        SharedPref.deleteShared(this);
        if (!isFinishing()) {
            dismissProgressDialog();
            UtilityMethods.showSnackbar(failureMessage, binding.getRoot(), LoginActivity.this);
        }
    }

    @Override
    public void onServerResultFailure(String failureMessage) {
        serverFailure(failureMessage);

    }
}
