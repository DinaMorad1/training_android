package byteis.com.trainingandroid.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.databinding.ActivityMainBinding;
import byteis.com.trainingandroid.fragments.BarChartFragment;
import byteis.com.trainingandroid.fragments.LineChartFragment;
import byteis.com.trainingandroid.fragments.PieChartFragment;
import byteis.com.trainingandroid.fragments.TableFragment;

public class MainActivity extends BaseActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_main, null, false);
        inflateContent(binding.getRoot());
        setToolBarContent("Home", true, false);
        replaceFragment(new BarChartFragment(), BarChartFragment.class.getName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle() != null) {
            String title = item.getTitle().toString();
            if (title.equals(getString(R.string.sign_out))) {
                signout();
                return true;
            } else if (title.equals(getString(R.string.bar_chart))) {
                replaceFragment(new BarChartFragment(), BarChartFragment.class.getName());
                return true;
            } else if (title.equals(getString(R.string.line_chart))) {
                replaceFragment(new LineChartFragment(), LineChartFragment.class.getName());
                return true;
            } else if (title.equals(getString(R.string.pie_chart))) {
                replaceFragment(new PieChartFragment(), PieChartFragment.class.getName());
                return true;
            } else if (title.equals(getString(R.string.products_table))) {
                replaceFragment(new TableFragment(), TableFragment.class.getName());
                return true;
            }
        }
        return true;
    }

    private void replaceFragment(Fragment fragment, String fragmentClassName) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(binding.frameLayout.getId(), fragment, fragmentClassName);
        fragmentTransaction.commit();
    }
}
