package byteis.com.trainingandroid.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.data.SharedPref;
import byteis.com.trainingandroid.databinding.ActivitySplashBinding;

public class SplashActivity extends BaseActivity {

//    private ActivitySplashBinding binding;

    private ActivitySplashBinding binding;
    private final int SPLASH_DISPLAY_LENGTH = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                /*handle activity to another activity navigation view on UI thread.*/
                    SplashActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                navigateActivity();
                            } catch (RuntimeException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void navigateActivity() {
        if (SharedPref.LoadString(SplashActivity.this, SharedPref.SESSION_ID) != null && !"".equals(SharedPref.LoadString(SplashActivity.this, SharedPref.SESSION_ID).trim()))
            startActivityWithTransition(this, new Intent(this, MainActivity.class));
        else
            startActivityWithTransition(this, new Intent(this, LoginActivity.class));
        finishActivity(this);
    }
}
