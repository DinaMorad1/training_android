package byteis.com.trainingandroid.data;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import byteis.com.trainingandroid.models.User;

/**
 * Created by Dina on 09/10/2016.
 */

public class TrainingApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initActiveAndroid();
    }

    private void initActiveAndroid() {
        Configuration.Builder configurationBuilder = new Configuration.Builder(this);
        configurationBuilder.addModelClasses(User.class);
        ActiveAndroid.initialize(configurationBuilder.create());
    }
}
