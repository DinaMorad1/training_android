package byteis.com.trainingandroid.serializer_adapter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import byteis.com.trainingandroid.models.User;

/**
 * Created by Dina on 09/10/2016.
 */

public class UserAdapter implements JsonSerializer<User> {

    @Override
    public JsonElement serialize(User user, Type type, JsonSerializationContext jsc) {
        JsonObject jsonObject = new JsonObject();
        if (user.getEmail() != null)
            jsonObject.addProperty("email", user.getEmail());

        if (user.getPassword() != null)
            jsonObject.addProperty("password", user.getPassword());

        return jsonObject;
    }
}
