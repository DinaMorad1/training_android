package byteis.com.trainingandroid.async;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import byteis.com.odooapilibrary.models.Order;
import byteis.com.trainingandroid.listeners.ChartListener;
import byteis.com.trainingandroid.managers.SortingManager;

/**
 * Created by Dina on 18/10/2016.
 */

public class RenderOrdersHashAsyncTask extends AsyncTask {

    private HashMap<String, Double> chartHashMap = new HashMap<>();
    private ArrayList<Order> ordersList;
    private JSONArray ordersJsonArray;
    private Gson gson = new Gson();
    private ChartListener chartListener;

    public RenderOrdersHashAsyncTask(JSONArray ordersJsonArray, ChartListener chartListener) {
        this.ordersJsonArray = ordersJsonArray;
        this.chartListener = chartListener;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        Type collectionType = new TypeToken<List<Order>>() {
        }.getType();

        ordersList = gson.fromJson(ordersJsonArray.toString(), collectionType);

        for (Order order : ordersList) {
            if (chartHashMap.get(order.getpartnerName()) != null)
                chartHashMap.put(order.getpartnerName(), chartHashMap.get(order.getpartnerName()) + order.getTotalPrice());
            else
                chartHashMap.put(order.getpartnerName(), order.getTotalPrice());
        }
        chartHashMap = SortingManager.sortOrdersHash(chartHashMap);
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (chartListener != null)
            chartListener.notifyChartHashMap(chartHashMap);
    }
}
