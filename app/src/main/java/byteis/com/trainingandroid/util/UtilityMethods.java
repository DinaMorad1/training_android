package byteis.com.trainingandroid.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.util.Random;

import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.models.Product;

/**
 * Created by Dina on 09/10/2016.
 */

public class UtilityMethods {

    public final static String ODOO_DATABASE_NAME = "OdooDatabase";

    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static Snackbar showSnackbar(final String snackbarMessage, View contentView, Context context) {
        Snackbar snackbar = Snackbar.make(contentView, snackbarMessage, Snackbar.LENGTH_SHORT);
        styleSnackbar(snackbar, context);
        hideKeypad(context);
        snackbar.show();
        return snackbar;
    }

    private static void styleSnackbar(Snackbar snackbar, Context context) {
        ViewGroup group = (ViewGroup) snackbar.getView();
        group.setBackgroundColor(ActivityCompat.getColor(context, R.color.primary_color));
        snackbar.setActionTextColor(Color.parseColor("#E6E6E6"));
        TextView actionTextView = (TextView) group.findViewById(R.id.snackbar_action);
        actionTextView.setTextSize(18);
        actionTextView.setTypeface(null, Typeface.BOLD);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
    }

    private static void hideKeypad(Context context) {
        View view = ((Activity) context).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static Product[] getProductsArray() {
        Product[] productsArray = {new Product("title1", "description1", "test comp1", "5", "egypt"),
                new Product("title2", "description2", "test comp2", "10", "egypt"),
                new Product("title3", "description3", "test comp3", "15", "egypt"),
                new Product("title4", "description4", "test comp4", "25", "egypt"),
                new Product("title5", "description5", "test comp5", "45", "egypt"),
                new Product("title6", "description6", "test comp6", "65", "egypt"),
                new Product("title7", "description7", "test comp7", "75", "egypt"),
                new Product("title8", "description8", "test comp8", "95", "egypt")};
        return productsArray;
    }
}
