package byteis.com.trainingandroid.listeners;

import java.util.HashMap;

/**
 * Created by Dina on 18/10/2016.
 */

public interface ChartListener {
    void notifyChartHashMap(HashMap<String, Double> chartHashMap);
}
