package byteis.com.trainingandroid.custom.json;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CustomJSONObjectRequest extends JsonObjectRequest {

    public CustomJSONObjectRequest(int method, String url, JSONObject jsonRequest,
                                   Response.Listener<JSONObject> successListener,
                                   Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, successListener, errorListener);
    }

    /*
     * Set request or API calls headers.
     *
     * @return headers: A hash map contains required headers.
     */
    @Override
    public Map<String, String> getHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        /*A header that requests a "json" type and "utf-8" encoded response.*/
        headers.put("Content-Type", "application/json; charset=utf-8;");
        return headers;
    }
}
