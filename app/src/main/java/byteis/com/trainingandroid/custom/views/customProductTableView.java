package byteis.com.trainingandroid.custom.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;

import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.comparators.ProductComparators;
import byteis.com.trainingandroid.models.Product;
import de.codecrafters.tableview.SortableTableView;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import de.codecrafters.tableview.toolkit.SortStateViewProviders;
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders;

/**
 * Created by Dina on 12/10/2016.
 */

public class CustomProductTableView extends SortableTableView<Product> {
    public CustomProductTableView(final Context context) {
        this(context, null);
    }

    public CustomProductTableView(final Context context, final AttributeSet attributes) {
        this(context, attributes, android.R.attr.listViewStyle);
    }

    public CustomProductTableView(final Context context, final AttributeSet attributes, final int styleAttributes) {
        super(context, attributes, styleAttributes);

        final SimpleTableHeaderAdapter simpleTableHeaderAdapter = new SimpleTableHeaderAdapter(context, R.string.product_title, R.string.product_description);
        simpleTableHeaderAdapter.setTextColor(ContextCompat.getColor(context, R.color.white));
        simpleTableHeaderAdapter.setHeaderGravity(Gravity.CENTER);
        setHeaderAdapter(simpleTableHeaderAdapter);

        final int rowColorEven = ContextCompat.getColor(context, R.color.table_data_row_even);
        final int rowColorOdd = ContextCompat.getColor(context, R.color.table_data_row_odd);
        setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(rowColorEven, rowColorOdd));
        setHeaderSortStateViewProvider(SortStateViewProviders.brightArrows());

        /*adjusting product title and description columns weight*/
        setColumnWeight(0, 2);
        setColumnWeight(1, 4);

        /*giving comparator enables sorting table columns*/
        setColumnComparator(0, ProductComparators.getProductTitleComparator());
        setColumnComparator(1, ProductComparators.getProductDescriptionComparator());
    }

}
