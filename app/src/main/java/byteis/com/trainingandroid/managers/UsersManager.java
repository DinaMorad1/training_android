package byteis.com.trainingandroid.managers;

import com.activeandroid.query.Delete;

import byteis.com.trainingandroid.models.User;

/**
 * Created by Dina on 09/10/2016.
 */

public class UsersManager {

    private static UsersManager instance;

    private UsersManager() {

    }

    public static UsersManager getInstance() {
        if (instance == null)
            instance = new UsersManager();
        return instance;
    }

    public void removeLoginUser() {
        new Delete().from(User.class).execute();
    }


}
