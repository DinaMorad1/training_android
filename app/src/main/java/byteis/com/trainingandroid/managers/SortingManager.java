package byteis.com.trainingandroid.managers;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Dina on 18/10/2016.
 */

public class SortingManager {

    public static HashMap<String, Double> sortOrdersHash(HashMap<String, Double> unsoredtMap) {

        List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(unsoredtMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
            public int compare(Map.Entry<String, Double> o1,
                               Map.Entry<String, Double> o2) {

                return o1.getKey().compareToIgnoreCase(o2.getKey());
            }
        });

        // Maintaining insertion order with the help of LinkedList
        HashMap<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
}
