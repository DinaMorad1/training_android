package byteis.com.trainingandroid.fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Random;

import byteis.com.odooapilibrary.api.ApiClient;
import byteis.com.odooapilibrary.api.ApiClientImp;
import byteis.com.odooapilibrary.api.ClientCallback;
import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.async.RenderOrdersHashAsyncTask;
import byteis.com.trainingandroid.listeners.ChartListener;
import byteis.com.trainingandroid.util.UtilityMethods;

/**
 * Created by Dina on 18/10/2016.
 */

public abstract class BaseFragment extends Fragment implements ClientCallback, ChartListener {

    protected ApiClient apiClient;
    protected HashMap<String, Double> chartHashMap = new HashMap<>();
    private ProgressDialog progressDialog;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiClient = ApiClientImp.getInstance(getActivity(), UtilityMethods.ODOO_DATABASE_NAME);
    }

    protected void getData() {
        apiClient.getSaleOrders(this, false, 0);
        showProgressDialog(getActivity().getString(R.string.please_wait), false, true);
    }


    @Override
    public void onServerResultSuccess(Object object) {
        JSONArray ordersJsonArray = (JSONArray) object;
        new RenderOrdersHashAsyncTask(ordersJsonArray, this).execute((Object) null);
    }

    @Override
    public void onServerResultFailure(String failureMessage) {
        UtilityMethods.showSnackbar(failureMessage, getParentView(), getActivity());
        dismissProgressDialog();
    }

    protected abstract View getParentView();

    @Override
    public void notifyChartHashMap(HashMap<String, Double> chartHashMap) {
        this.chartHashMap = chartHashMap;
        notifyChartHashMapUpdated();
        dismissProgressDialog();
    }

    protected abstract void notifyChartHashMapUpdated();

    protected void showProgressDialog(String dialogMessage, boolean isCancelable, boolean isIndeteminate) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(getActivity());

        if (!progressDialog.isShowing()) {
            progressDialog.setMessage(dialogMessage);
            progressDialog.setCancelable(isCancelable);
            progressDialog.setIndeterminate(isIndeteminate);
            progressDialog.show();
        }
    }

    protected void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    protected int getRandomColor() {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        return color;
    }
}
