package byteis.com.trainingandroid.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.charts.BarChart;
import byteis.com.chartsuserlib.managers.BarChartManager;
import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.databinding.FragmentBarchartBinding;

/**
 * Created by Dina on 11/10/2016.
 */

public class BarChartFragment extends BaseFragment {

    private FragmentBarchartBinding binding;
    private BarChart barChart;
    private BarChartManager builder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.fragment_barchart, null, false);
        this.barChart = binding.barChart;
        builder = new BarChartManager(barChart);
        builder.initChartView().initAxis().initLegend();
        getData();
        return binding.getRoot();
    }

    @Override
    protected void notifyChartHashMapUpdated() {
        builder.setHashMap(chartHashMap);
        builder.initHash();
        builder.notifyDataChanged();
    }

    @Override
    protected View getParentView() {
        return binding.getRoot();
    }
}
