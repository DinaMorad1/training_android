package byteis.com.trainingandroid.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import byteis.com.chartsuserlib.managers.PieChartManager;
import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.databinding.FragmentPiechartBinding;

/**
 * Created by Dina on 11/10/2016.
 */

public class PieChartFragment extends BaseFragment implements OnChartValueSelectedListener {

    private FragmentPiechartBinding binding;
    private PieChart pieChart;
    private PieChartManager chartManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.fragment_piechart, null, false);
        pieChart = binding.pieChart;
        pieChart.setOnChartValueSelectedListener(this);
        chartManager = new PieChartManager(pieChart);
        chartManager.initLegend().initChartView();
        getData();
        return binding.getRoot();
    }

    @Override
    protected View getParentView() {
        return binding.getRoot();
    }

    @Override
    protected void notifyChartHashMapUpdated() {
        chartManager.setHashMap(chartHashMap);
        chartManager.initHash();
        chartManager.notifyDataChanged();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;



    }

    @Override
    public void onNothingSelected() {

    }
}
