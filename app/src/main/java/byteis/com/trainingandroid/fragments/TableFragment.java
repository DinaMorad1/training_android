package byteis.com.trainingandroid.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.adapters.OrdersAdapter;
import byteis.com.trainingandroid.databinding.FragmentTableBinding;

/**
 * Created by Dina on 12/10/2016.
 */

public class TableFragment extends BaseFragment {

    private FragmentTableBinding binding;
    private OrdersAdapter ordersAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.fragment_table, null, false);
        initProductsTable();
        showNoItems();
        getData();
        return binding.getRoot();
    }

    private void initProductsTable() {
        ordersAdapter = new OrdersAdapter(getActivity(), chartHashMap);
        binding.table.setAdapter(ordersAdapter);
    }

    @Override
    protected View getParentView() {
        return binding.getRoot();
    }

    @Override
    protected void notifyChartHashMapUpdated() {
        if (chartHashMap.size() > 0) {
            ordersAdapter.setOrdersHashMap(chartHashMap);
            ordersAdapter.notifyDataSetChanged();
            showTableItems();
        } else {
            showNoItems();
        }
    }

    private void showNoItems() {
        binding.noItemsTextView.setVisibility(View.VISIBLE);
        binding.layoutTableView.setVisibility(View.GONE);
    }

    private void showTableItems() {
        binding.noItemsTextView.setVisibility(View.GONE);
        binding.layoutTableView.setVisibility(View.VISIBLE);
    }
}
