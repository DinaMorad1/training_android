package byteis.com.trainingandroid.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.charts.LineChart;
import byteis.com.chartsuserlib.managers.LineChartManager;
import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.databinding.FragmentLinechartBinding;

/**
 * Created by Dina on 11/10/2016.
 */

public class LineChartFragment extends BaseFragment {

    private FragmentLinechartBinding binding;
    private LineChart lineChart;
    private LineChartManager builder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.fragment_linechart, null, false);
        lineChart = binding.lineChart;
        builder = new LineChartManager(lineChart, getActivity());
        builder.initChartView().initAxis().initLegend();
        getData();
        return binding.getRoot();
    }

    @Override
    protected View getParentView() {
        return binding.getRoot();
    }

    @Override
    protected void notifyChartHashMapUpdated() {
        builder.setHashMap(chartHashMap);
        builder.initHash();
        builder.notifyDataChanged();
    }
}
