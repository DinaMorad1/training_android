package byteis.com.trainingandroid.comparators;

import java.util.Comparator;

import byteis.com.trainingandroid.models.Product;

/**
 * Created by Dina on 12/10/2016.
 */

public class ProductComparators {

    private ProductComparators() {

    }

    public static Comparator<Product> getProductTitleComparator() {
        return new ProductTitleComparator();
    }

    public static Comparator<Product> getProductDescriptionComparator() {
        return new ProductDescriptionComparator();
    }

    private static class ProductTitleComparator implements Comparator<Product> {

        @Override
        public int compare(final Product product1, final Product product2) {
            return product1.getTitle().compareTo(product2.getTitle());
        }
    }

    private static class ProductDescriptionComparator implements Comparator<Product> {

        @Override
        public int compare(final Product product1, final Product product2) {
            return product1.getDescription().compareTo(product2.getDescription());
        }
    }
}
