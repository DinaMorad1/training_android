package byteis.com.trainingandroid.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;

import byteis.com.trainingandroid.R;
import byteis.com.trainingandroid.models.Product;
import byteis.com.trainingandroid.util.UtilityMethods;

/**
 * Created by Dina on 12/10/2016.
 */

public class OrdersAdapter extends BaseTableAdapter {

    private Context context;
    private float density;
    private final String headers[] = {
            "Customer",
            "Sales Total"
    };
    private HashMap<String, Double> ordersHashMap;


    public OrdersAdapter(Context context, HashMap<String, Double> ordersHashMap) {
        this.context = context;
        density = context.getResources().getDisplayMetrics().density;
        this.ordersHashMap = ordersHashMap;
    }

    public void setOrdersHashMap(HashMap<String, Double> ordersHashMap) {
        this.ordersHashMap = ordersHashMap;
        notifyDataSetChanged();
    }

    @Override
    public int getRowCount() {
        return ordersHashMap.size();
    }

    @Override
    public int getColumnCount() {
        return headers.length - 1;
    }

    @Override
    public View getView(int row, int column, View convertView, ViewGroup parent) {
        View view = null;
        switch (getItemViewType(row, column)) {
            case 0:
                view = getFirstHeader(row, column, convertView, parent);
                break;
            case 1:
                view = getHeader(row, column, convertView, parent);
                break;
            case 2:
                if (column == -1) {
                    view = getItemCustomer(row, column, convertView, parent);
                } else
                    view = getItemTotalPrice(row, column, convertView, parent);
                break;
        }
        return view;
    }

    @Override
    public int getWidth(int column) {
        return Math.round(100 * density);
    }

    @Override
    public int getHeight(int row) {
        return Math.round(35 * density);
    }

    @Override
    public int getItemViewType(int row, int column) {
        int itemViewType = 1;
        if (row == -1 && column == -1)
            itemViewType = 0;
        else if (row == -1)
            itemViewType = 1;
        else
            itemViewType = 2;
        return itemViewType;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    private View getFirstHeader(int row, int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.item_table_header, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.header_text_view)).setText(headers[0]);
        return convertView;
    }

    private View getHeader(int row, int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.item_table_header, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.header_text_view)).setText(headers[column + 1]);
        return convertView;
    }

    private View getItemCustomer(int row, int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.item_table, parent, false);
        }

        Object ordersCustomer = ordersHashMap.keySet().toArray()[row];
        ((TextView) convertView.findViewById(R.id.item_title)).setText(ordersCustomer.toString());
        return convertView;
    }

    private View getItemTotalPrice(int row, int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.item_table_right_divider, parent, false);
        }

        Object ordersCustomer = ordersHashMap.keySet().toArray()[row];
        double ordersTotalPrice = ordersHashMap.get(ordersCustomer);
        NumberFormat myformatter = new DecimalFormat("###########.#");
        String totalPriceStr = myformatter.format(ordersTotalPrice);
        ((TextView) convertView.findViewById(R.id.item_title)).setText(totalPriceStr);
        return convertView;
    }


    private View getItem(int row, int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.item_table, parent, false);
        }

        /*the customer is the key of the hashmap and total price is the value*/
        Object ordersCustomer = ordersHashMap.keySet().toArray()[row];
        double ordersTotalPrice = ordersHashMap.get(ordersCustomer);
        NumberFormat myformatter = new DecimalFormat("###########.#");
        String totalPriceStr = myformatter.format(ordersTotalPrice);
        String textStr = "";

        switch (column) {
            case -1:
                textStr = ordersCustomer.toString();
                break;
            case 0:
                textStr = totalPriceStr;
                break;
        }

        ((TextView) convertView.findViewById(R.id.item_title)).setText(textStr);
        return convertView;
    }

}
