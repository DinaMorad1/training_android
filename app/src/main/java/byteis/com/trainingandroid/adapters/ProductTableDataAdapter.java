package byteis.com.trainingandroid.adapters;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import byteis.com.trainingandroid.models.Product;
import de.codecrafters.tableview.TableDataAdapter;

/**
 * Created by Dina on 12/10/2016.
 */

public class ProductTableDataAdapter extends TableDataAdapter<Product> {

    private static final int TEXT_SIZE = 14;

    public ProductTableDataAdapter(Context context, Product[] data) {
        super(context, data);
    }

    @Override
    public View getCellView(int rowIndex, int columnIndex, ViewGroup parentView) {
        final Product product = getRowData(rowIndex);
        View renderedView = null;

        switch (columnIndex) {
            case 0:
                renderedView = renderProductTitle(product);
                break;
            case 1:
                renderedView = renderProductDescription(product);
                break;
        }

        return renderedView;
    }

    private View renderProductTitle(Product product) {
        return renderString(product.getTitle());
    }

    private View renderProductDescription(Product product) {
        return renderString(product.getDescription());
    }

    private View renderString(final String value) {
        final TextView textView = new TextView(getContext());
        textView.setText(value);
        textView.setPadding(20, 10, 20, 10);
        textView.setTextSize(TEXT_SIZE);
        textView.setMaxLines(2);
        textView.setGravity(Gravity.CENTER);
        return textView;
    }


}
