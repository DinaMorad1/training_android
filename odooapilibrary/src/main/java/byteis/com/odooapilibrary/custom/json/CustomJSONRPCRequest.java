package byteis.com.odooapilibrary.custom.json;

import com.android.volley.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dina on 23/10/2016.
 */

public class CustomJSONRPCRequest extends JsonRpcRequest {
    public CustomJSONRPCRequest(String url, String method, JSONObject jsonRequest, Response.Listener listener, Response.ErrorListener errorListener, int requestId) throws JSONException {
        super(url, method, jsonRequest, listener, errorListener, requestId);
    }

    @Override
    public Map<String, String> getHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        /*A header that requests a "json" type and "utf-8" encoded response.*/
        headers.put("Content-Type", "application/json; charset=utf-8;");
        return headers;
    }
}
