package byteis.com.odooapilibrary.api;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import byteis.com.odooapilibrary.data.SharedPref;
import byteis.com.odooapilibrary.managers.EncryptionManager;
import byteis.com.odooapilibrary.models.Order;

/**
 * Created by Dina on 23/10/2016.
 */

public class ApiClientImp extends VolleyHelper implements ApiClient {

    private static ApiClientImp instance;
    private String odooDatabaseName;

    private ApiClientImp(Context context, String odooDatabaseName) {
        super(context);
        this.odooDatabaseName = odooDatabaseName;
    }

    public static ApiClientImp getInstance(Context context, String odooDatabaseName) {
        if (instance == null)
            instance = new ApiClientImp(context, odooDatabaseName);
        return instance;
    }

    @Override
    public void odooLogin(String username, String password, ClientCallback<JSONObject> callback) {
        String url = ODOO_BASE_URL + ODOO_LOGIN_URL;
        JSONObject params = new JSONObject();

        try {
            params.put("db", odooDatabaseName);
            params.put("login", username);
            params.put("password", password);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        scheduleJsonRpcObjectRequest("call", url,
                params, callback, JSONObject.class, NORMAL_TIME_OUT);
    }

    public void getSaleOrders(ClientCallback<Order> callback, boolean limit, int offset) {
        String url = ODOO_BASE_URL + ODOO_JSON_RPC;

        /*search criteria filter*/
        JSONArray methodParams = new JSONArray();
        JSONArray paramsArray = new JSONArray();

        /*filter to render parameters with confirmed state*/
        JSONArray itemsJsonArray = new JSONArray();
        itemsJsonArray.put("state");
        itemsJsonArray.put("=");
        itemsJsonArray.put("confirmed");
        paramsArray.put(itemsJsonArray);
        methodParams.put(paramsArray);

        /*needed fields to be returned from the model*/
        JSONArray showItems = new JSONArray();
        showItems.put("price_total");//"amount_total");
        showItems.put("product_id");
        showItems.put("partner_id");
        showItems.put("date_confirm");
        showItems.put("state");

        getList(methodParams, showItems, url, limit, offset, "sale.report", callback, JSONObject.class);
    }

    private void getList(JSONArray searchJsonArray, JSONArray modelParamsArray, String url, boolean limit, int offset, String modelName, ClientCallback callback, Class classType) {
        JSONObject params = new JSONObject();

        try {
            /*needed fields to be returned from the model*/
            JSONObject kw = new JSONObject();
            kw.put("fields", modelParamsArray);

            /*offset and limit if needed for pagination*/
            kw.put("limit", limit);
            kw.put("offset", offset);

            /*service and method*/
            params.put("service", "object");
            params.put("method", "execute_kw");

            /*database and user*/
            JSONArray allItemsJsonArray = new JSONArray();
            allItemsJsonArray.put(odooDatabaseName);//database name
            allItemsJsonArray.put(SharedPref.LoadInt(context, SharedPref.USER_ID));/*user id*/
            allItemsJsonArray.put(EncryptionManager.getPassword(context));/*user password*/

            /*model name*/
            allItemsJsonArray.put(modelName);//model

            /*method name*/
            allItemsJsonArray.put("search_read");//method
            allItemsJsonArray.put(searchJsonArray);//search criteria
            allItemsJsonArray.put(kw);
            params.put("args", allItemsJsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonRpcObjectRequest("call", url,
                params, callback, classType, NORMAL_TIME_OUT);
    }
}
