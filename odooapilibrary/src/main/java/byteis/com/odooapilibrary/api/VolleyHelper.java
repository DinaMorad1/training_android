package byteis.com.odooapilibrary.api;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.util.Random;

import byteis.com.odooapilibrary.R;
import byteis.com.odooapilibrary.custom.json.CustomJSONRPCRequest;
import byteis.com.odooapilibrary.custom.json.JsonRpcResponse;

/**
 * Created by Dina on 23/10/2016.
 */

public class VolleyHelper {
    protected Context context;
    protected RequestQueue mRequestQueue;
    public final static int NORMAL_TIME_OUT = 30000;


    protected VolleyHelper(Context context) {
        this.context = context;
        mRequestQueue = Volley.newRequestQueue(context);
    }

    protected void scheduleJsonRpcObjectRequest(String method, String requestUrl, JSONObject jsonObject, final ClientCallback clientCallback, final Class<?> classType, int requestTimeout) {
        try {
            CustomJSONRPCRequest jsonrpcRequest = new CustomJSONRPCRequest(requestUrl, method, jsonObject, new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    try {
                        if (response != null) {
                            JsonRpcResponse rpcResponse = new JsonRpcResponse(response.toString());
                            if (rpcResponse != null) {
                                if (rpcResponse.getResult() != null) {
                                    Object resultJsonObj = new JsonRpcResponse(((JSONObject) response).toString()).getResult();
                                    clientCallback.onServerResultSuccess(resultJsonObj);
                                } else if (rpcResponse.getError() != null) {
                                    JSONObject errorJsonObject = rpcResponse.getError();
                                    String serverErrorMessage = context.getString(R.string.something_wrong);
                                    if (errorJsonObject.optString("message") != null) {
                                        serverErrorMessage = errorJsonObject.optString("message");
                                    }
                                    clientCallback.onServerResultFailure(serverErrorMessage);
                                }
                            }
                        }
//                        Object resultJsonObj = new JsonRpcResponse(((JSONObject) response).toString()).getResult();
//                        clientCallback.onServerResultSuccess(resultJsonObj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    volleyServerError(error, clientCallback);
                }
            }, getRequestID());

            jsonrpcRequest.setRetryPolicy(new
                    DefaultRetryPolicy(requestTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            mRequestQueue.add(jsonrpcRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int getRequestID() {
        return Math.abs((new Random()).nextInt(9999));
    }

    public static Object getGsonObj(String responseStr, Class<?> classType) {
        Gson gson = customizeGsonBuilder().create();
        return gson.fromJson(responseStr, classType);
    }

    private void volleyServerError(VolleyError error, ClientCallback clientCallback) {
        clientCallback.onServerResultFailure(context.getString(R.string.something_wrong));
    }

    public static GsonBuilder customizeGsonBuilder() {

        return new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT,
                        Modifier.STATIC);
    }

    public void addToQueue(Request request, String tag) {
        request.setTag(tag);
        mRequestQueue.add(request);
    }
}
