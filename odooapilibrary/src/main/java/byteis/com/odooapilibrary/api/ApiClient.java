package byteis.com.odooapilibrary.api;

import org.json.JSONObject;

import byteis.com.odooapilibrary.models.Order;

/**
 * Created by Dina on 23/10/2016.
 */

public interface ApiClient {
    /*Odoo Fields*/
    String ODOO_BASE_URL = "http://e45d880b.ngrok.io";
    String ODOO_LOGIN_URL = "/web/session/authenticate";
    String ODOO_SEARCH_URL = "/web/dataset/search_read";
    String ODOO_JSON_RPC = "/jsonrpc";

    void odooLogin(String username, String password, ClientCallback<JSONObject> callback);

    void getSaleOrders(ClientCallback<Order> callback, boolean limit, int offset);

}
