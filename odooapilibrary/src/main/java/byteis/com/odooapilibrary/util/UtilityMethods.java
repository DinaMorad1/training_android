package byteis.com.odooapilibrary.util;

import java.util.Random;

/**
 * Created by Dina on 23/10/2016.
 */

public class UtilityMethods {
    
    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static byte[] createRandomKey(Random r) {
        byte[] key = new byte[16];
        for (int i = 0; i < 16; ++i) {
            int j = 97 + r.nextInt(122 - 97);
            char c = (char) j;
            key[i] = (byte) c;
        }
        return key;
    }
}
