package byteis.com.chartsuserlib.managers;

import android.graphics.Color;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import byteis.com.chartsuserlib.managers.formatter.OrderXAxisValueFormatter;

/**
 * Created by Dina on 24/10/2016.
 */

public class BarChartManager extends BaseChartManager {

    private ArrayList<BarEntry> yVals1 = new ArrayList<>();

    public BarChartManager(BarLineChartBase chart) {
        super(chart);
    }

    @Override
    public BaseChartManager initAxis() {
        /*Customizing x axis*/
        XAxis xAxis = chart.getXAxis();
        xAxis.setDrawGridLines(false);/*preventing drawing x axis grid lines*/
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);

        /*Customizing y axis*/
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.enableGridDashedLine(15f, 15f, 0f);

        /*Removing the text from the right axis to be seen in the left one only*/
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setTextSize(0f);
        rightAxis.setTextColor(Color.TRANSPARENT);
        rightAxis.enableGridDashedLine(15f, 15f, 0f);

        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        rightAxis.setAxisMinimum(0f);
        leftAxis.setSpaceTop(5f);
        rightAxis.setSpaceTop(5f);
        return this;
    }

    @Override
    public BaseChartManager initLegend() {
        chart.getLegend().setCustom(new LegendEntry[]{});
        return this;
    }

    @Override
    public BaseChartManager initChartView() {
        chart.getDescription().setEnabled(false);
        return this;
    }

    @Override
    public void initHash() {
        Iterator it = hashMap.entrySet().iterator();
        int index = 0;
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            yVals1.add(new BarEntry((index + 1), ((Number) pair.getValue()).floatValue(), getRandomColor()));
            index++;
        }
    }

    public void notifyDataChanged() {
        XAxis xAxis = chart.getXAxis();
        IAxisValueFormatter xAxisFormatter = new OrderXAxisValueFormatter(chart, hashMap);
        xAxis.setValueFormatter(xAxisFormatter);
        BarDataSet set1;
        if (chart.getData() == null || chart.getData().getDataSetCount() == 0) {
            setData();
        } else {
            set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            chart.getData().notifyDataChanged();
        }

        chart.notifyDataSetChanged();
        chart.invalidate();
    }

    public void setData() {
        chart.getXAxis().setLabelCount(hashMap.size());
        BarDataSet set1 = new BarDataSet(yVals1, "Expenses Chart");
        set1.setColors(new int[]{Color.RED, Color.GREEN});

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(0.9f);
        chart.setData(data);
        chart.setVisibleXRangeMaximum(2);
    }

}
