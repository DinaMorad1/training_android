package byteis.com.chartsuserlib.managers.formatter;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dina on 24/10/2016.
 */

public class OrderXAxisValueFormatter implements IAxisValueFormatter {

    ArrayList<String> arList = new ArrayList<String>();

    private BarLineChartBase<?> chart;

    public OrderXAxisValueFormatter(BarLineChartBase<?> chart, HashMap<String, Double> chartHashMap) {
        this.chart = chart;

        for (Map.Entry<String, Double> map : chartHashMap.entrySet()) {
            arList.add(map.getKey());
        }
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return arList.get((int) (value - 1));
    }

    @Override
    public int getDecimalDigits() {
        return 0;
    }
}
