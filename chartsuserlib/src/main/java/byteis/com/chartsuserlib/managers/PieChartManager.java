package byteis.com.chartsuserlib.managers;

import android.graphics.Color;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Dina on 24/10/2016.
 */

public class PieChartManager extends BaseChartManager {

    private PieChart pieChart;
    private ArrayList<PieEntry> entries = new ArrayList<>();

    public PieChartManager(PieChart pieChart) {
        super();
        this.pieChart = pieChart;
    }

    @Override
    public BaseChartManager initAxis() {
        return this;
    }

    @Override
    public BaseChartManager initLegend() {
        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setTextColor(Color.BLACK);
        l.setYOffset(0f);
        l.setForm(Legend.LegendForm.CIRCLE);
        return this;
    }

    @Override
    public BaseChartManager initChartView() {
        pieChart.getDescription().setEnabled(false);

        // undo all highlights
        pieChart.highlightValues(null);

        pieChart.setEntryLabelTextSize(8f);
        return this;
    }

    @Override
    public void initHash() {
        Iterator it = hashMap.entrySet().iterator();
        int totalOrdersValueSum = 0;
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            totalOrdersValueSum += ((Number) pair.getValue()).floatValue();
        }

        it = hashMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            entries.add(new PieEntry((((Number) pair.getValue()).floatValue() / totalOrdersValueSum) * 100.0f, pair.getKey()));
        }
        setData();
    }

    private void setData() {
        PieDataSet dataSet = new PieDataSet(entries, "Orders Pie Chart");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        List<Integer> colors = initColors(new ArrayList<Integer>());
        dataSet.setColors(colors);

        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);


        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(8f);
        data.setValueTextColor(Color.BLACK);
        pieChart.setData(data);
        pieChart.invalidate();
    }

    private List<Integer> initColors(List<Integer> colors) {
        for (int i = 0; i < hashMap.size(); i++)
            colors.add(getRandomColor());
        return colors;
    }

    public void notifyDataChanged() {
        PieDataSet set1;
        if (pieChart.getData() == null || pieChart.getData().getDataSetCount() == 0) {
            setData();
        } else {
            set1 = (PieDataSet) pieChart.getData().getDataSetByIndex(0);
            set1.setValues(entries);
            pieChart.getData().notifyDataChanged();
        }

        pieChart.notifyDataSetChanged();
        pieChart.invalidate();
    }

}
