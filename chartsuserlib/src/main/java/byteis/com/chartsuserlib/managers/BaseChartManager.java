package byteis.com.chartsuserlib.managers;

import android.graphics.Color;

import com.github.mikephil.charting.charts.BarLineChartBase;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by Dina on 24/10/2016.
 */

public abstract class BaseChartManager {
    protected BarLineChartBase chart;
    protected HashMap<String, Double> hashMap = new HashMap<>();

    public BaseChartManager(BarLineChartBase chart) {
        this.chart = chart;
    }

    public BaseChartManager() {
    }

    public BarLineChartBase getChart() {
        return chart;
    }

    public abstract BaseChartManager initAxis();

    public abstract BaseChartManager initLegend();

    public abstract BaseChartManager initChartView();

    public abstract void initHash();

    protected int getRandomColor() {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        return color;
    }

    public void setHashMap(HashMap<String, Double> hashMap) {
        this.hashMap = hashMap;
    }
}
