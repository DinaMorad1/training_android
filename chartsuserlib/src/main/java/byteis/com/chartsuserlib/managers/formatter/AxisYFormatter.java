package byteis.com.chartsuserlib.managers.formatter;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by Dina on 24/10/2016.
 */

public class AxisYFormatter implements IAxisValueFormatter
{

    private DecimalFormat mFormat;

    public AxisYFormatter() {
        mFormat = new DecimalFormat("###,###,###,##0.0");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value) + " LE";
    }

    @Override
    public int getDecimalDigits() {
        return 1;
    }
}
