package byteis.com.chartsuserlib.managers;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.ActivityCompat;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import byteis.com.chartsuserlib.R;
import byteis.com.chartsuserlib.managers.formatter.OrderXAxisValueFormatter;

/**
 * Created by Kimo on 24/10/2016.
 */

public class LineChartManager extends BaseChartManager {

    private ArrayList<Entry> vals = new ArrayList<>();
    private Context context;

    public LineChartManager(BarLineChartBase chart, Context context) {
        super(chart);
        this.context = context;
    }

    @Override
    public BaseChartManager initAxis() {
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setDrawZeroLine(false);
        leftAxis.enableGridDashedLine(15f, 15f, 0f);
        leftAxis.setAxisMinimum(0f);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.enableGridDashedLine(15f, 15f, 0f);
        /*Disable Right axis text*/
        rightAxis.setTextSize(0f);
        rightAxis.setTextColor(Color.TRANSPARENT);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.enableGridDashedLine(15f, 15f, 0f);
        xAxis.enableGridDashedLine(15f, 15f, 0f);
        return this;
    }

    @Override
    public BaseChartManager initLegend() {
        chart.getLegend().setCustom(new LegendEntry[]{});
        return this;
    }

    @Override
    public BaseChartManager initChartView() {
        chart.getDescription().setEnabled(false);
        return this;
    }

    public void initHash() {
        Iterator it = hashMap.entrySet().iterator();
        int index = 0;
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            vals.add(new Entry((index + 1), ((Number) pair.getValue()).floatValue(), R.color.color_green));/*bar color*/
            index++;
        }
        setData();
    }

    private void setData() {
        LineDataSet lineDataSet = new LineDataSet(vals, "Orders");
        lineDataSet.setColor(ActivityCompat.getColor(context, R.color.color_green));
        lineDataSet.setCircleColor(ActivityCompat.getColor(context, R.color.color_red));
        lineDataSet.setValueTextColor(ActivityCompat.getColor(context, R.color.primary_color_dark));

        // create a data object with the datasets
        LineData data = new LineData(lineDataSet);

        // set data
        chart.setData(data);
        chart.setVisibleXRangeMaximum(2);
    }

    public void notifyDataChanged() {
        XAxis xAxis = chart.getXAxis();
        IAxisValueFormatter custom = new OrderXAxisValueFormatter(chart, hashMap);
        xAxis.setValueFormatter(custom);
        xAxis.setGranularityEnabled(true);

        LineDataSet set1;
        if (chart.getData() == null || chart.getData().getDataSetCount() == 0) {
            setData();
        } else {
            set1 = (LineDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(vals);
            chart.getData().notifyDataChanged();
        }

        chart.notifyDataSetChanged();
        chart.invalidate();
    }
}
